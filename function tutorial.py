'''
Example:
Define a function called “perimeter_of_rectangle” that takes two numbers to  represent the length and width of a rectangle and returns the perimeter of the rectangle.  (Remember that the perimeter is twice the length added to twice the width). 
(From '5083 - Python for KS4 v1.1', page 39)'''

def perimeter_of_rectangle(length,width): # defines the function and parameters
	area = length*width #performs the calculation
	return area # sends the value out of the function to be used
	
print (perimeter_of_rectangle(10,10)) # calls the function with length 10 and width 10 and prints the returned value.

# Tasks: try calling the function with text what happens (eg hello,hello)?
# Try calling the function with 3 other integer values, does it work?
# What would : print (perimeter_of_rectangle(10*10,10)) output?










''' Task create a function called "area_of_a_triangle" that takes the relevant parameters (base, height) and returns the area of a triangle
Use a print command to show the function running.
Put your code below here
'''




''' Task create a function called "area_of_a_circle" that takes the relevant parameters (radius) and returns the area of a circle
Use a print command to show the function running.
Put your code below here
'''




''' Task create a function called "long_text" that takes a string as a parameter  and returns "Long text" if the length of the string is over 10 characters.
Use a print command to show the function running.
use len(string) to find the length of the string.
Use IF and ELSE 
Put your code below here
'''
#template

#def long_text(string):#
	# check the length
	#IF 
	#	return ???
	#ELSE
	#	return ???

#print(function_name("string"))
